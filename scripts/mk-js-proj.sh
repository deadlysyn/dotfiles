#!/usr/bin/env bash
#
# Run from new node project directory.
# https://blog.echobind.com/integrating-prettier-eslint-airbnb-style-guide-in-vscode-47f07b5d7d6a

npm install -D eslint prettier

#./node_modules/.bin/eslint --init
cp "${HOME}/src/dotfiles/.eslintrc.yml" .
cp "${HOME}/src/dotfiles/.eslintignore" .
cp "${HOME}/src/dotfiles/.prettierrc" .
cp "${HOME}/src/dotfiles/.prettierignore" .

npx install-peerdeps --dev eslint-config-airbnb
npm install -D eslint-config-prettier eslint-plugin-prettier

