" set leader to , vs \
let mapleader = ","

" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplete#close_popup() . "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()

" Netrw Style Listing
let g:netrw_liststyle = 3

nnoremap <Leader><Leader> :Tlist<CR><C-W>h<C-W>s:e .<CR><C-W>l:let g:netrw_chgwin=winnr()<CR><C-W>h

" format with goimports instead of gofmt
let g:go_fmt_command = "goimports"

" use camel vs snake case
let g:go_addtags_transform = "camelcase"

" run metalinter on save
let g:go_metalinter_autosave = 1
" what to run when calling GoMetaLinter
let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']

" Enable filetype plugins
filetype plugin on

" settings for powerline
set guifont=Inconsolata\ for\ Powerline:h14
let g:Powerline_symbols = 'fancy'
set encoding=utf-8
set termencoding=utf-8
set t_Co=256
set term=xterm-256color
set fillchars+=stl:\ ,stlnc:\
set laststatus=2
" set showtabline=2
set noshowmode

" cursor highlight
set cursorline
set cursorcolumn

" Smart indenting after certain words
set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

" Folds using indents.  Useful in python.
" Type 'za' to open/close folds
"set foldmethod=indent
"set foldlevel=99

" Fixes syntax errors in .sh
let is_bash=1

" More syntax highlighting.
let python_highlight_all = 1

"Syntax rules
syntax enable
colorscheme solarized
set background=dark

set hlsearch
hi LineNr term=none ctermfg=none

" Highlight end of line whitespace.
highlight WhitespaceEOL ctermbg=red guibg=red
match WhitespaceEOL /\s\+$/

set t_kb=
fixdel

" turn off auto adding comments on next line
" so you can cut and paste reliably
" http://vimdoc.sourceforge.net/htmldoc/change.html#fo-table
set fo=tcq
set nocompatible
set modeline

" Set up go syntax highlighting
au BufRead,BufNewFile *.go
 \ set filetype=go
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_generate_tags = 1

" Enable indentation
filetype plugin indent on

set ai ic sm showmode hls warn nu
set tabstop=4 softtabstop=4 expandtab shiftwidth=4 smarttab
"set textwidth=79

" VIM Specific Commands
set incsearch showcmd backspace=indent,eol,start
set list
set listchars=tab:>-

" cursor highlight
set cursorline
set cursorcolumn

" Smart indenting after certain words
set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

" don't check for go-vim binaries on each run to speed things up
let g:go_disable_autoinstall = 0

" write file contents when calling :make
set autowrite

" make jumping between errors in go-vim easier
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>

" make common go tasks easier in go-vim
au FileType go nmap <Leader>c <Plug>(go-coverage-toggle)

au FileType go nmap <Leader>i <Plug>(go-info)
let g:go_auto_type_info = 1
set updatetime=100

au FileType go nmap <leader>r  <Plug>(go-run)
au FileType go nmap <leader>t  <Plug>(go-test)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap gd <Plug>(go-def-tab)

let g:go_auto_sameids = 1

" run :GoBuild or :GoTestCompile based on the go file
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#test#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction

autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>

" ? make all lists quickfix (vs quickfix+location which use different keys)
let g:go_list_type = "quickfix"

" timeout for go test
let g:go_test_timeout = '30s'

" treat ejs templates like html
au BufNewFile,BufRead *.ejs set filetype=html

" use two space tabs for yaml
au FileType yaml setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2

" make yank, etc go to standard clipboard
set clipboard=unnamed
