# dotfiles

Much of my bash/vim/tmux/etc configs are cribbed from others, but I keep local customizations here.  For any of these to be useful, first install/consider the following:

- https://github.com/powerline/powerline
- https://github.com/tony/tmux-config
- https://github.com/fatih/vim-go

# Resources

- https://powerline.readthedocs.io/en/latest
- [tmux color chart](https://superuser.com/questions/285381/how-does-the-tmux-color-palette-work/1104214#1104214)
- https://github.com/tony/vim-config-framework
- https://github.com/tmux-plugins/tmux-battery
- https://github.com/robhurring/tmux-spotify
